var express = require('express');
var app = express();
var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var User = require('../models/user');

passport.use(new localStrategy(
    function(username, password, done){
        User.findOne({ username: username, password: password}, function(err, user){
            if(err){
                return done(err);
            }
            if(!user) {
                return done(null, false, { message: 'Incorrect username'});
            }
            return done(null, user);
        });
    }
));

passport.serializeUser(function(user, done){
    done(null, user.id);
});

passport.deserializeUser(function(id, done){
    User.findById(id, function(err, user){
        done(err, user);
    });
});

var auth = function(req, res, next){
    if(!req.isAuthenticated())
        res.sendStatus(401);
    else
        next();
}

app.get('/', function(req, res){
    res.render('index', {
        title: 'Passport JS and Angular app',
        message: 'HElllo jackass :D'
    });
});

app.get('/users', auth, function(req, res){
    User.find(function(err, users){
        if(err){
            res.send(err);
        } else {
            res.json(users);
        }
    });
});

app.get('/loggedin', function(req, res){
    res.send(req.isAuthenticated() ? req.user : '0');
});

app.post('/login', passport.authenticate('local', {
    successRedirect:'/api/u/users',
    failureRedirect: '/api/u/'
}));

app.post('/logout', function(req, res){
    req.logOut();
    res.sendStatus(200);
});

module.exports = {app: app, passport: passport};