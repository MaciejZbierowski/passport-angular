/* jshint node: true */
var express = require('express');
var app = express();
var httpServer = require('http').Server(app);
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressSession = require('express-session');
var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var port = process.env.PORT || 3000;
var path = require('path');

var mongoose = require('mongoose');
var configDB = require('./config/database');
var User = require('./models/user');
mongoose.connect(configDB.url);
mongoose.connection.on('open', function(){
    console.log('Connected to MongoDB');
});
mongoose.connection.on('error', function(){
    console.err.bind(console, 'MongoDB Error');
});

app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(expressSession({
    secret: 'securedSession',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(express.static(path.join(__dirname, 'public')));

var usersRoutes = require('./routes/users');
app.use('/api/u', usersRoutes.app);

httpServer.listen(port, function(){
    console.log('localhost:' + port);
});